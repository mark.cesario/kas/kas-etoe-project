import argparse
import jinja2
import os, sys
import configparser
import hvac
from dataclasses import dataclass




def parse_args(args):
    """
    Parse command line arguments

    :param args: command args

    :returns: argparse args
    """
    parser = argparse.ArgumentParser(description="Setup Vault for GitLab to Handle GKE")
    parser.add_argument('--glfile', type=str, help='The path of the file with your GITLAB token.')
    parser.add_argument('--gcpfile', type=str, help='The path to the b64 copy of your GCP service account json.')
    parser.add_argument('--pid', type=int, help='The id of the GitLab project which will use these creds')
    parser.add_argument('--pname', type=str, help='The name of the GitLab project')
    pargs = parser.parse_args(args)
    return pargs

def main(**kwargs):
    '''
    Generate the source object list then pass it to the jinja2 template to render the pipeline.
    :param argv: n/a
    :return: n/a
    '''
    vc = hvac.Client()
    
    glsa=os.path.basename(kwargs.get('glfile'))
    glt=os.path.splitext(os.path.basename(kwargs.get('gcpfile')))[0]

    policy_rendered = jinja2.Template(open('./templates/policy.json.j2').read()).render(GCP_SERVICE_ACCOUNT=glsa, GITLAB_TOKEN_ID=glt)

    project_name=kwargs.get('pname')
    vc.sys.create_or_update_policy(
        name=project_name,
        policy=policy_rendered
    )

    glpid=kwargs.get('pid')

    vc.auth.jwt.create_role(
        name=project_name,
        allowed_redirect_uris="https://gitlab.com",
        role_type='jwt',
        user_claim='user_email',
        token_explicit_max_ttl=60,
        bound_claims={
            'project_id': glpid
        }
    )

if __name__ == "__main__":
    args = parse_args(sys.argv[1:])
    sys.exit(main(**vars(args)))
