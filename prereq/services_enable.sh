#!/bin/bash
######
#
# This script sets your required service account policy bindings so you can use the Terraform in this project.
# Example call:
# bash ./services_enable.sh -p jsandlin-c9fe7132
######

# ARGPARSE
while getopts p: flag
do
    case "${flag}" in
        p) GCP_PROJECT=${OPTARG};;
    esac
done

declare -a ServiceArray=("apigateway.googleapis.com" "cloudkms.googleapis.com" "cloudresourcemanager.googleapis.com" "compute.googleapis.com" "container.googleapis.com" "containerregistry.googleapis.com" "iam.googleapis.com" "iamcredentials.googleapis.com" "secretmanager.googleapis.com" "servicecontrol.googleapis.com" "servicemanagement.googleapis.com" "serviceusage.googleapis.com")

for s in ${ServiceArray[@]}; do
    gcloud services enable --project ${GCP_PROJECT} $s
done


##################################
# Now let's echo out our services for this account.
##################################
gcloud services list --enabled