#!/bin/bash -xv
######
#
# This script sets your required service account policy bindings so you can use the Terraform in this project.
# Example call:
# bash ./iam_policy_binding.sh -p jsandlin-c9fe7132 -a jsandlin-admin
######
while getopts a:p: flag
do
    case "${flag}" in
        p) GCP_PROJECT=${OPTARG};;
        a) SERVICE_ACCOUNT_NAME=${OPTARG};;
    esac
done

# REF
# https://cloud.google.com/iam/docs/understanding-roles#basic

# ROLE: resourcemanager.projectIamAdmin
# DESCRIPTION: Provides permissions to administer IAM policies on projects.
gcloud projects add-iam-policy-binding $GCP_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT_NAME@$GCP_PROJECT.iam.gserviceaccount.com" --role="roles/resourcemanager.projectIamAdmin"

# google_container_cluster.primary: Creating...
# 2022-02-18T13:04:56.006-0500 [ERROR] vertex “google_container_cluster.primary” error: googleapi: Error 403: Required “container.clusters.create” permission(s) for “projects/745422394672”., forbidden
# ╷
# │ Error: googleapi: Error 403: Required “container.clusters.create” permission(s) for “projects/745422394672”., forbidden
# │
# │  with google_container_cluster.primary,
# │  on gke_cluster.tf line 6, in resource “google_container_cluster” “primary”:
# │  6: resource “google_container_cluster” “primary” {
# │
# ╵
gcloud projects add-iam-policy-binding $GCP_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT_NAME@$GCP_PROJECT.iam.gserviceaccount.com" --role="roles/container.clusterAdmin"

# google_container_cluster.primary: Creating...
# 2022-02-18T13:18:24.114-0500 [ERROR] vertex “google_container_cluster.primary” error: googleapi: Error 400: The user does not have access to service account “745422394672-compute@developer.gserviceaccount.com”. Ask a project owner to grant you the iam.serviceAccountUser role on the service account., badRequest
# ╷
# │ Error: googleapi: Error 400: The user does not have access to service account “745422394672-compute@developer.gserviceaccount.com”. Ask a project owner to grant you the iam.serviceAccountUser role on the service account., badRequest
# │
# │  with google_container_cluster.primary,
# │  on gke_cluster.tf line 6, in resource “google_container_cluster” “primary”:
# │  6: resource “google_container_cluster” “primary” {
# │

gcloud projects add-iam-policy-binding $GCP_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT_NAME@$GCP_PROJECT.iam.gserviceaccount.com" --role="roles/iam.serviceAccountAdmin"

# This is too much in privs. Try to not use.
# Full control of all Compute Engine resources.
# Replace with compute.instanceAdmin?
gcloud projects add-iam-policy-binding $GCP_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT_NAME@$GCP_PROJECT.iam.gserviceaccount.com" --role="roles/compute.admin"

gcloud projects add-iam-policy-binding $GCP_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT_NAME@$GCP_PROJECT.iam.gserviceaccount.com" --role="roles/iam.serviceAccountUser"


# Permissions to create, modify, and delete virtual machine instances. This includes permissions to create, modify, and delete disks, and also to configure Shielded VM settings.
# https://cloud.google.com/iam/docs/understanding-roles#compute-engine-roles
gcloud projects add-iam-policy-binding $GCP_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT_NAME@$GCP_PROJECT.iam.gserviceaccount.com" --role="roles/compute.instanceAdmin.v1"

# Provides full access to Cloud KMS resources, except encrypt and decrypt operations.
# https://cloud.google.com/iam/docs/understanding-roles#cloud-kms-roles
gcloud projects add-iam-policy-binding $GCP_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT_NAME@$GCP_PROJECT.iam.gserviceaccount.com" --role="roles/cloudkms.admin"

# Provides ability to use Cloud KMS resources for encrypt and decrypt operations only.
gcloud projects add-iam-policy-binding $GCP_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT_NAME@$GCP_PROJECT.iam.gserviceaccount.com" --role="roles/cloudkms.cryptoKeyEncrypterDecrypter"

# Provides access to all custom roles in the project.
gcloud projects add-iam-policy-binding $GCP_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT_NAME@$GCP_PROJECT.iam.gserviceaccount.com" --role="roles/iam.roleAdmin"

# Impersonate service accounts (create OAuth2 access tokens, sign blobs or JWTs, etc).
gcloud projects add-iam-policy-binding $GCP_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT_NAME@$GCP_PROJECT.iam.gserviceaccount.com" --role="roles/iam.serviceAccountTokenCreator"

# Permissions to create, modify, and delete networking resources, except for firewall rules and SSL certificates.
#   The network admin role allows read-only access to firewall rules, SSL certificates, and instances (to view their ephemeral IP addresses).
#   The network admin role does not allow a user to create, start, stop, or delete instances.
gcloud projects add-iam-policy-binding $GCP_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT_NAME@$GCP_PROJECT.iam.gserviceaccount.com" --role="roles/compute.networkAdmin"

# Full access to policies, access levels, and access zones
# https://cloud.google.com/iam/docs/understanding-roles#access-context-manager-roles
gcloud projects add-iam-policy-binding $GCP_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT_NAME@$GCP_PROJECT.iam.gserviceaccount.com" --role="roles/accesscontextmanager.policyAdmin"

# Create and manage service accounts.
# https://cloud.google.com/iam/docs/understanding-roles#service-accounts-rolegcloud projects add-iam-policy-binding $GCP_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT_NAME@$GCP_PROJECT.iam.gserviceaccount.com" --role="roles/iam.serviceAccountAdmin"

#mark added
#gcloud projects add-iam-policy-binding $GCP_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT_NAME@$GCP_PROJECT.iam.gserviceaccount.com" --role="roles/iam.computeAdmin"
#gcloud projects add-iam-policy-binding $GCP_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT_NAME@$GCP_PROJECT.iam.gserviceaccount.com" --role="roles/iam.computeNetworkAdmin"
#gcloud projects add-iam-policy-binding $GCP_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT_NAME@$GCP_PROJECT.iam.gserviceaccount.com" --role="roles/iam.kubernetesEngineAdmin"
#gcloud projects add-iam-policy-binding $GCP_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT_NAME@$GCP_PROJECT.iam.gserviceaccount.com" --role="roles/iam.serviceAccountAdmin"
#gcloud projects add-iam-policy-binding $GCP_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT_NAME@$GCP_PROJECT.iam.gserviceaccount.com" --role="roles/iam.serviceAccounts.create"

# Error: clusterroles.rbac.authorization.k8s.io "certmanager-cert-manager-controller-approve:cert-manager-io" is forbidden: user "system:serviceaccount:gitlab-kubernetes-agent:gitlab-agent" (groups=["system:serviceaccounts" "system:serviceaccounts:gitlab-kubernetes-agent" "system:authenticated"]) is attempting to grant RBAC permissions not currently held:
#  {APIGroups:["cert-manager.io"], Resources:["signers"], ResourceNames:["clusterissuers.cert-manager.io/*"], Verbs:["approve"]}
#  {APIGroups:["cert-manager.io"], Resources:["signers"], ResourceNames:["issuers.cert-manager.io/*"], Verbs:["approve"]}

##################################
# Now let's echo out our policies for this account.
##################################
gcloud projects get-iam-policy  $GCP_PROJECT \
    --flatten="bindings[].members" \
    --format="table(bindings.role)" \
    --filter="bindings.members:$SERVICE_ACCOUNT_NAME"
